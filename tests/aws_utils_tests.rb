require './aws_utils.rb'

aws = AWSUtils.new()

instance = aws.start_instance("i-0211e893ad99c5e9e")

############## Test SCP ##############

SSHUtils.scp_upload(instance.public_ip_address, "./aws_utils_test.rb", "/home/ubuntu")

#####################################

############## Test SSH ##############

results = SSHUtils.execute(instance.public_ip_address, "ls /home/ubuntu")
puts "-------"
puts "Results"
puts "-------"
puts "#{results}"
puts "-------"
puts "\n\n"

#####################################

### Test getting client public ip ###

puts "Client IP: "+SSHUtils.get_client_public_ip()

#####################################

code = aws.get_instance_code("i-0211e893ad99c5e9e")
puts "AWS instance code: #{code}"

instance_name = aws.get_instance_name_by_id("i-0211e893ad99c5e9e")
puts "Instance name: #{instance_name}"

instance_id = aws.get_instance_id_by_name(instance_name)
puts "Instance id: #{instance_id}"
