require './ssh_utils.rb'

class RunAllTests
    def initialize
        config = YAML.load_file('client_config.yml')
        @remark_dir = config['remark_dir']
        @master_node_dir = config['master_node_dir']
        @signals_dir = config['signals_dir']
        @master_node_ip = config['master_node_ip']
        @client_remark_dir = config['client_remark_dir']
    end

    def get_list_of_files
        results = SSHUtils.execute(@master_node_ip, "ls #{@master_node_dir}/OUTPUT/")
        arr = results.split("\n")
        return arr
    end

    def download_files
        list_of_files = get_list_of_files()
        list_of_files.each do |f|
            SSHUtils.scp_download(@master_node_ip, "#{@master_node_dir}/OUTPUT/#{f}", "#{@client_remark_dir}/OUTPUT/")
            SSHUtils.scp_download(@master_node_ip, "#{@master_node_dir}/OUTPUT.FIXED/#{f}", "#{@client_remark_dir}/OUTPUT.FIXED/")
            SSHUtils.scp_download(@master_node_ip, "#{@master_node_dir}/OUTPUT.INTERNAL/#{f}", "#{@client_remark_dir}/OUTPUT.INTERNAL/")
        end
    end

    def clear_previous_results
        results = SSHUtils.execute(@master_node_ip, "rm #{@master_node_dir}/OUTPUT/*")
        results = SSHUtils.execute(@master_node_ip, "rm #{@master_node_dir}/OUTPUT.FIXED/*")
        results = SSHUtils.execute(@master_node_ip, "rm #{@master_node_dir}/OUTPUT.INTERNAL/*")
    end

    def send_start_signal(git_branch)
        results = SSHUtils.execute(@master_node_ip, "cd #{@master_node_dir}/signals; touch start; touch branch; echo \"#{git_branch}\" > branch")
        puts results
    end

    def clear_signals
        results = SSHUtils.execute(@master_node_ip, "cd #{@master_node_dir}/signals; rm done; rm branch")
    end

    def get_current_git_branch
        results = `cd #{@client_remark_dir} & git status`
        arr = results.split("\n")
        line = arr[0]
        branch_name = line.gsub("On branch ","")
        return branch_name
    end

    def run(git_branch = nil)   
        
        # TODO maybe move to master node ?
        clear_previous_results()

        if (git_branch == nil) then
            git_branch = get_current_git_branch()
        end 
        puts "Git branch: #{git_branch}"       
        
        puts "... Started ..."
        send_start_signal(git_branch)

        puts "... Running ..."

        while (true) do

            if SSHUtils.check_for_signal(@master_node_ip, "done") then                
                puts "... Downloading the processed files ..."

                download_files()
                
                clear_signals()

                break
            end
            
            sleep(5)
        end
        puts "... Done ..."
    end 

end

r= RunAllTests.new

if (ARGV[1]) then
   puts "Error: git branch name should not contain spaces."
   return
end

r.run(ARGV[0])
