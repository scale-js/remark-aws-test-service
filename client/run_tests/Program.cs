﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace run_tests
{
    class Program
    {
        static string master_node; //= "54.174.104.12";
        static string master_node_dir; // = "/home/ubuntu/REMARK_TESTS";
        static string client_remark_dir; // = "C:\\REMARK\\AutoStyle\\ReMarkTest";
        static string signals_dir; // = "/home/ubuntu/REMARK_TESTS/signals";

        static void load_config(string fileName)
        {
            var lines = File.ReadAllLines(fileName);
            for (var i = 0; i < lines.Length; i += 1)
            {
                var line = lines[i];
                if (line.IndexOf(":") > 0)
                {
                    string key = line.Substring(0, line.IndexOf(":")).Trim();
                    string value = line.Substring(line.IndexOf(":") + 1).Trim();

                    //Console.WriteLine("[" + key + "] = [" + value + "]");

                    switch (key)
                    {
                        case "master_node_ip":
                            master_node = value;
                            break;
                        case "master_node_dir":
                            master_node_dir = value;
                            break;
                        case "client_remark_dir":
                            client_remark_dir = value;
                            break;
                        case "signals_dir":
                            signals_dir = value;
                            break;
                        default:
                            break;                           
                    }
                }
            }

            //Console.WriteLine("master_node=" + master_node);
            //Console.WriteLine("master_node-dir=" + master_node_dir);
            //Console.WriteLine("client_remark_dir=" + client_remark_dir);
            //Console.WriteLine("signals_dir=" + signals_dir);
        }

        static string execute(string command, string parameters)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            
            process.StartInfo.FileName = command;
            process.StartInfo.Arguments = parameters;
            
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.UseShellExecute = false;

            process.Start();
         
            var stdOutput = new StringBuilder();
            process.OutputDataReceived += (sender, args) => stdOutput.AppendLine(args.Data); 

            string stdError = null;
            try
            {
                process.Start();
                process.BeginOutputReadLine();
                stdError = process.StandardError.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }

            return stdOutput.ToString();
        }

        static string execute_(string command, string parameters)
        {
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = command;

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = parameters;

            pProcess.StartInfo.UseShellExecute = false;

            //Set output of program to be written to process output stream
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Optional
            //pProcess.StartInfo.WorkingDirectory = strWorkingDirectory;

            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            return strOutput;
        }


        static string ssh(string command_with_parameters)
        {
            //Console.WriteLine("SSH: " + command_with_parameters);
            return execute("ssh", "-i WinRemark-1.pem ubuntu@" + master_node + " '" + command_with_parameters + "'");
        }

        static string scp(string remote_file_path, string local_dir)
        {
            return execute("scp", " -i WinRemark-1.pem ubuntu@" + master_node + ":" + remote_file_path+ " " +local_dir);
        }

        static String[] get_list_of_files()
        {
            string res = ssh("ls " + master_node_dir + "/OUTPUT/");
            String[] arr = res.Split('\n');
            return arr;
        }

        static void download_files()
        {
            String[] arr = get_list_of_files();
            foreach (string f in arr)
            {
                if (!f.Equals(""))
                {
                    scp(master_node_dir+"/OUTPUT/" + f, client_remark_dir+"\\OUTPUT\\");
                    scp(master_node_dir + "/OUTPUT.FIXED/" + f, client_remark_dir + "\\OUTPUT.FIXED\\");
                    scp(master_node_dir + "/OUTPUT.INTERNAL/" + f, client_remark_dir + "\\OUTPUT.INTERNAL\\");
                }
            }
        }

        static void clear_previous_results()
        {
            string res = ssh("rm " + master_node_dir + "/OUTPUT/*");
            res = ssh("rm " + master_node_dir + "/OUTPUT.FIXED/*");
            res = ssh("rm " + master_node_dir + "/OUTPUT.INTERNAL/*");
        }

        static void send_start_signals(string git_branch, string test_branch)
        {
            string res = ssh("cd "+master_node_dir+ "/signals; touch start; touch branch; printf \"" + git_branch + "\n"+ test_branch + "\" > branch");
        }

        static void clear_signals()
        {
            string res = ssh("cd " + master_node_dir + "/signals;  rm done; rm branch; rm start;");
        }

        static bool check_for_signals(string signal)
        {
            //Console.WriteLine("check_for_signals: " + signal);
            string res = ssh("ls " + signals_dir +  "/" + signal);
            //Console.WriteLine("Run: " + "ls " + signals_dir + "/" + signal);
            //Console.WriteLine("Result: " + res);
            return (res.Contains(signal));
        }
        static string get_current_git_branch()
        {
            string res = execute("get_git_branch.bat", "\""+client_remark_dir+"\"");

            String[] arr = res.Split('\n');

            foreach (string line in arr)
            {
                if (line.Contains("On branch"))
                {
                    return line.Replace("On branch ", "");
                }
            }

            return null;
        }
        static bool git_branch_exists(string branch_name)
        {
            string res = execute("check_branch.bat", "\"" + client_remark_dir + "\"");
            
            String[] arr = res.Split('\n');
           
            foreach (string line in arr)
            {
                if (line.Contains(branch_name))
                {
                    return true;
                }
            }

            return false;
        }


        static void run(string run_branch, string test_branch)
        {            
            clear_previous_results();

            string git_branch = get_current_git_branch();
            Console.WriteLine("Git <current> branch: " + git_branch);

            if (run_branch != null)
            {
                git_branch = run_branch;
            }

            Console.WriteLine("Git <run> branch: " + git_branch);

            Console.WriteLine("... Started ...");

            send_start_signals(git_branch, test_branch);

            Console.WriteLine("... Running ...");

            while(true)
            {
               if (check_for_signals("done"))
                {
                    //Console.WriteLine("... Downloading the processed files ...");

                    download_files();

                    Console.WriteLine("Done.");

                    clear_signals();

                    return;
                }
               // Thread.Sleep(5000);
            }
        }
        static void Main(string[] args)
        {
            string run_branch = null;
            string test_branch = null;
            bool run_branch_exists = false;
            bool test_branch_exists = false;

            load_config("client_config.yml");

            if (args.Length >= 1)
            {
                run_branch = args[0];
                run_branch_exists = git_branch_exists(run_branch);

                if (run_branch_exists)
                {
                    //Console.WriteLine("Git <run> branch " + run_branch + " already exists.");
                }
                else
                {
                   // Console.WriteLine("Fatal error: Git <run> branch " + run_branch + " does not exist.");
                  //  return;
                }
            }

            if (args.Length == 2)
            {
                test_branch = args[1];
                test_branch_exists = git_branch_exists(test_branch);

                if (test_branch_exists)
                {
                    //Console.WriteLine("Git <test> branch " + test_branch + " already exists.");
                }
                else
                {
                    //Console.WriteLine("Git <test> branch " + test_branch + " does not exist.");
                }
            }
            var watch = System.Diagnostics.Stopwatch.StartNew();

            run(run_branch, test_branch);

            watch.Stop();
          
            TimeSpan t = TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds);
            string timing = string.Format("{1:D2}m:{2:D2}s",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds,
                                    t.Milliseconds);
            Console.WriteLine("Total execution time: " + timing);
        }
    }
}
