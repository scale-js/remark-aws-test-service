require 'yaml'
require 'net/ssh'
require 'net/scp'
require 'net/http'

class SSHUtils

    ################## SETTINGS #####################

    config = YAML.load_file('config.yml')

    MASTER_NODE_USERNAME = config['master_node_username']
    PEM_FILE = config['pem_file']
    SIGNALS_DIR = config['signals_dir']

    #################################################
    
    def self.get_client_public_ip 
        public_ip = Net::HTTP.get URI "https://api.ipify.org"
        return public_ip
    end

    def self.execute(ip_address, command)
        stdout = ""
        Net::SSH.start(ip_address, MASTER_NODE_USERNAME, :keys => PEM_FILE) do|ssh|
            #puts "Connected via SSH to #{ip_address}"		
            ssh.exec!(command) do |channel, stream, data|
              stdout << data if stream == :stdout 
            end
            #puts stdout
        end	
        return stdout
    end

    def self.scp_download(ip_address, source, target)
        Net::SCP.start(ip_address, MASTER_NODE_USERNAME, :keys => PEM_FILE) do |scp|
            # run multiple downloads in parallel
            dn= scp.download(source, target)
            [dn].each { |d| d.wait }
        end
    end

    def self.scp_upload(ip_address, source, target)
        Net::SCP.start(ip_address, MASTER_NODE_USERNAME, :keys => PEM_FILE) do |scp|
            # run multiple uploads in parallel
            dn = scp.upload(source, target)
            [dn].each { |d| d.wait }
        end
    end

    def self.check_for_signal(ip, signal)
        results = execute(ip, "ls #{SIGNALS_DIR}/#{signal}")
        if (results.match(/#{Regexp.escape(signal)}/)) then
            return true
        end
        return false
    end
    
    def self.send_done_signal(ip)
        results = execute(ip, "cd #{SIGNALS_DIR}; rm start; rm range; touch done")
    end

end