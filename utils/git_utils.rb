class GitUtils
    def self.get_git_status(dir)
        results = `cd #{dir} & git status`
        arr = results.split("\n")
        return arr
    end
 
    def self.undo_git_changes(dir)
         results = `cd #{dir} & git checkout .`
    end
 
    def self.sync_with_git(dir, branch)
         results = `cd #{dir} & git fetch origin & git checkout #{branch} & git pull origin #{branch}`
    end
end     