require 'aws-sdk-ec2' 
require 'yaml'


class AWSUtils

################## SETTINGS #####################

config = YAML.load_file('config.yml')
AWS_KEY = config['aws_key']
AWS_SECRET_KEY = config['aws_secret_key']
REGION = config['region']

Aws.config.update({
	region: REGION,
	credentials: Aws::Credentials.new(AWS_KEY, AWS_SECRET_KEY)
})

##################################################

def self.get_aws_code_as_string(code) 
	return "PENDING" if code == 0
	return "RUNNING" if code == 16
	return "SHUTTING-DOWN" if code == 32
	return "TERMINATED" if code == 48
	return "STOPPPING" if code == 64
	return "STOPPED ..." if code == 80
end

def self.get_instance(id)
	ec2 = Aws::EC2::Resource.new(region: REGION)	      
	i = ec2.instance(id)
	return i
end

def self.self.get_instance_code(id)
	i = get_instance(id)
	return i.state.code
end

def self.get_instance_name_by_id(id)
	i = get_instance(id)
	i.tags.each do |t|
	  if (t["key"] == "Name") then
	  	  return t["value"]
	  end
	end	
	return nil
end

def self.get_instance_id_by_name(name)
	ec2 = Aws::EC2::Resource.new(region: REGION)	
	ec2.instances.each do |i|
        instance_name = get_instance_name_by_id(i.id)
        return i.id if (instance_name == name)
    end
    return nil
end

def self.start_instance(instance_name)
	ec2 = Aws::EC2::Resource.new(region: REGION)

	ec2.instances.each do |ci|
        iname = get_instance_name_by_id(ci.id)
		id = nil
		if (instance_name == iname) then
			id = ci.id
			instance = ci
			break
	    end		
    end	

    if (i.state.code == 80) then		
      puts "Start new instance #{instance_name}"		
	  i.start	
	else 
	  puts "Can't start instance: instance is in state <"+get_aws_code_as_string(i.state.code)+">"   
	end
	return i
end

def self.stop_instance(instance_name)
	ec2 = Aws::EC2::Resource.new(region: REGION)	      
	i = ec2.instance(instance_name)

    if (i.state.code == 16) then		
      puts "Start new instance #{instance_name}"		
	  i.stop
	else 
	  puts "Can't stop instance: instance is in state <"+get_aws_code_as_string(i.state.code)+">"   	  	 
	end
	return i
end

#############################################

end
