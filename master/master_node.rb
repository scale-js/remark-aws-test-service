require 'aws-sdk-ec2' 
require 'yaml'

class MasterNode

    def initialize(config = nil)
        if (config == nil)
            config = YAML.load_file('master_config.yml')
        end
 
        aws_key = config['aws_key']
        aws_secret_key = config['aws_secret_key']
        @region = config['region']

        @always_stop_workers = config['always_stop_workers']
        @always_stop_workers =  (@always_stop_workers == "yes" || @always_stop_workers == nil) ? true : false

        @master_node_dir = config['master_node_dir']
        @signals_dir = config['signals_dir']
        
        @number_of_parallel_proc = config['number_of_parallel_proc']
        @number_of_parallel_proc ||= 25

        @polling_time = config['polling_time']
        @polling_time ||= 5
        
        @workers = config['workers'].split(" ")
        if (@workers.size > 0) then
            @max_subset = @number_of_parallel_proc * @workers.size
            puts "max_subset: #{@max_subset}"               
        else
            raise Exception.new "Error: workers are not defined"
        end

        Aws.config.update({
            region: @region,
            credentials: Aws::Credentials.new(aws_key, aws_secret_key)
        })
        @ec2 = Aws::EC2::Resource.new(region: @region)
    end

    def get_aws_code_as_string(code) 
        return "PENDING" if code == 0
        return "RUNNING" if code == 16
        return "SHUTTING-DOWN" if code == 32
        return "TERMINATED" if code == 48
        return "STOPPPING" if code == 64
        return "STOPPED ..." if code == 80
    end
    
    def get_instance(id)      
        i = @ec2.instance(id)
        return i
    end
    
    def get_instance_name_by_id(id)
        i = get_instance(id)
        i.tags.each do |t|
          if (t["key"] == "Name") then
                return t["value"]
          end
        end 
        return nil
    end   

    def get_instance_by_name(name)  
        @ec2.instances.each do |i|
            instance_name = get_instance_name_by_id(i.id)
            return i if (instance_name == name)
        end
        return nil
    end    

    def start_instance(instance_name)
        instance = get_instance_by_name(instance_name)
        if (instance.state.code == 80) then     
            puts "Start new instance #{instance_name}"      
            instance.start  
        else 
            puts "Can't start instance: instance is in state <"+get_aws_code_as_string(instance.state.code)+">"   
        end
    end

    def stop_instance(instance_name)
        instance = get_instance_by_name(instance_name)
        if (instance.state.code == 16) then     
            puts "Stop instance #{instance_name}"       
            instance.stop   
        else 
            if (instance.state.code != 80) then
                puts "Can't stop instance: instance is in state <"+get_aws_code_as_string(instance.state.code)+">"   
            end
        end
    end

    def start_workers
        @workers.each do |w|
            puts "Start worker: #{w}"
            start_instance(w)
        end
    end

    def stop_workers
        @workers.each do |w|
            puts "Stop worker: #{w}"
            stop_instance(w)
        end
    end

    def check_for_signal(dir, signal)
        results = `ls #{dir}`
        arr = results.split("\n")
        if arr.size > 0
            arr.each do |a|
                if a.match(/#{Regexp.escape(signal)}/)
                    return true
                end
            end
        end
        return false
    end

    def set_ranges
        from_subset = 1
        to_subset = 1
        
        @workers.each do |w|            
            to_subset = from_subset + @number_of_parallel_proc - 1 
            
            open("#{@signals_dir}/#{w}/range", 'w') { |f|
              f.puts "#{from_subset} #{to_subset} #{@max_subset}"
            }   

            from_subset = to_subset + 1
        end        
    end

    def all_done?
        not_all_done = false 
        @workers.each do |w|            
            if (check_for_signal("#{@signals_dir}/#{w}", "done")) then
                puts "Worker #{w}: done"                
            else 
                #puts "Worker #{w}: running ..."  
                not_all_done = true
                break 
            end
        end 
        return !not_all_done       
    end

    def clear_all_workers
        @workers.each do |w|
            results = `cd #{@signals_dir}/#{w}; rm done; rm range`
        end       
    end

    def clear_results
        results = `rm #{@master_node_dir}/OUTPUT/*`
        results = `rm #{@master_node_dir}/OUTPUT.FIXED/*`
        results = `rm #{@master_node_dir}/OUTPUT.INTERNAL/*`        
    end
        
    def run
        
        found_start_signal = false
        workers_already_started = false
         
        max_counter = 600/@polling_time
        counter = 0

        while (true) do            
            #############################
            counter += 1

            if (!workers_already_started) then
                found_start_signal = check_for_signal(@signals_dir, "start")
             end
                
            if (found_start_signal) then
                counter = 0
                #puts "... Clear previous results ..."
                #clear_results()
                
                #puts "... Running ..."

                if (!workers_already_started) then
                    puts "... Start workers ..."
                    
                    start_workers()
                    workers_already_started  = true
                end
                
                set_ranges()

                if (all_done?) then
                   puts "... All workers are done ..." 
                   results = `cd #{@signals_dir}; touch done; rm start`
                   
                   puts "... Clear all workers ..."
                   clear_all_workers()
                   
                   if @always_stop_workers then
                       puts "... Stop workers ..."
                       stop_workers()                        
                   end         
                   workers_already_started = false 
                end
            else 
                if (counter > max_counter) then
                    stop_workers()
                    counter = 0
                end
            end

            #############################
            
            sleep(@polling_time)
        end
    end

end

m = MasterNode.new
m.run()

