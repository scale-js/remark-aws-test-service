require 'rubygems'
require './ssh_utils.rb'
require './git_utils.rb'
require 'net/http'
require 'aws-sdk-ec2' 
require 'win32/daemon'
require 'fileutils'

require 'win32/service'
require 'rbconfig'
include Win32

class Worker < Daemon

    def initialize
        config = YAML.load_file('worker_config.yml')
        @remark_dir = config['remark_dir']
        @master_node_dir = config['master_node_dir']
        @signals_dir = config['signals_dir']
        @master_node_ip = config['master_node_ip']

        @polling_time = config['polling_time']
        @polling_time ||= 10        

        aws_key = config['aws_key']
        aws_secret_key = config['aws_secret_key']
        @region = config['region']

        @temp_dir = config['temp_dir']

        Aws.config.update({
            region: @region,
            credentials: Aws::Credentials.new(aws_key, aws_secret_key)
        })
        @ec2 = Aws::EC2::Resource.new(region: @region)

        instance_id = get_instance_id_of_this_instance()
        instance = @ec2.instance(instance_id)
        @instance_name = get_instance_name(instance)

        log "Current instance: #{@instance_name}"

        @signals_instance_dir = "#{@signals_dir}/#{@instance_name}"

        #log "SIGNALS directory: #{@signals_dir}"
        #log "SIGNALS worker directory: #{@signals_instance_dir}"

        @run_tests_cmd = config['run_tests_cmd']
    end

    def create_dir_if_not_exists(dir)
        if (!Dir.exists?(dir)) then
            FileUtils.mkdir_p(dir) 
        end    
    end

    def delete_all_files_in_dir(dir_path)
        Dir.foreach(dir_path) do |f|
            fn = File.join(dir_path, f)
            File.delete(fn) if f != '.' && f != '..'
        end    
    end

    def clear_temp_dirs
        begin        
            delete_all_files_in_dir("#{@temp_dir}/ReMarkTest/INPUT")
            delete_all_files_in_dir("#{@temp_dir}/ReMarkTest/INPUT.FIXED")
            delete_all_files_in_dir("#{@temp_dir}/ReMarkTest/OUTPUT")
            delete_all_files_in_dir("#{@temp_dir}/ReMarkTest/OUTPUT.FIXED")
            delete_all_files_in_dir("#{@temp_dir}/ReMarkTest/OUTPUT.INTERNAL")

            #temp_dir = @temp_dir.gsub!("/", "\\")      
            #system("del /q #{temp_dir}\\ReMarkTest\\INPUT\\*")
            #system("del /q #{temp_dir}\\ReMarkTest\\INPUT.FIXED\\*")
            #system("del /q #{temp_dir}\\ReMarkTest\\OUTPUT\\*")
            #system("del /q #{temp_dir}\\ReMarkTest\\OUTPUT.INTERNAL\\*")
            #system("del /q #{temp_dir}\\ReMarkTest\\OUTPUT.FIXED\\*")

        rescue Exception => e
            log_error(e.backtrace)
        end  
    end    

    def copy_all_to_dir(dir_name, target_dir)
        root_dir = dir_name
        
        current_dir = Dir.pwd
        begin
            Dir.chdir dir_name
            
            files = Dir['**/*']
            
            files.each do |f|
            if !Dir.exist?(f)
                source = "#{root_dir}/#{f}"       
                source.gsub!("/", "\\")
                target_dir.gsub!("/", "\\")
                system("copy \"#{source}\" \"#{target_dir}\"")
            end
            end
        rescue Exception => e
            Dir.chdir current_dir
        end
        Dir.chdir current_dir
    end  
    
    def copy_to_temp_dir(list_of_files)
        list_of_files.each do |f|
            begin
                fname = "#{@remark_dir}/#{f}"
                log "Copy file: #{fname} to temp directory"
                if f.match(/INPUT\//) then
                    log "Copy file: #{fname} to INPUT temp directory"
                    FileUtils.cp(fname, "#{@temp_dir}/ReMarkTest/INPUT")
                end                
                if f.match(/INPUT\.FIXED\//) then
                    log "Copy file: #{fname} to INPUT.FIXED temp directory"
                    FileUtils.cp(fname, "#{@temp_dir}/ReMarkTest/INPUT.FIXED")
                end
                if f.match(/OUTPUT\//) then
                    FileUtils.cp(fname, "#{@temp_dir}/ReMarkTest/OUTPUT")
                end
                if f.match(/OUTPUT\.FIXED\//) then
                    FileUtils.cp(fname, "#{@temp_dir}/ReMarkTest/OUTPUT.FIXED")
                end
                if f.match(/OUTPUT\.INTERNAL\//) then
                    FileUtils.cp(fname, "#{@temp_dir}/ReMarkTest/OUTPUT.INTERNAL")
                end    
            rescue => e
                log "Error:"
                log e.message
            end        
        end
    end  

    def copy_from_temp_dir(list_of_files)
        list_of_files.each do |f|
            begin
                fname = "#{@temp_dir}/#{f}"
                log "Copy file: #{fname} from temp directory to #{@remark_dir}"
                if f.match(/INPUT\//) then
                    FileUtils.cp(fname, "#{@remark_dir}/ReMarkTest/INPUT")
                end                  
                if f.match(/INPUT\.FIXED\//) then
                    FileUtils.cp(fname, "#{@remark_dir}/ReMarkTest/INPUT.FIXED")
                end                
                if f.match(/OUTPUT\//) then
                    FileUtils.cp(fname, "#{@remark_dir}/ReMarkTest/OUTPUT")
                end
                if f.match(/OUTPUT\.FIXED\//) then
                    FileUtils.cp(fname, "#{@remark_dir}/ReMarkTest/OUTPUT.FIXED")
                end
                if f.match(/OUTPUT\.INTERNAL\//) then
                    FileUtils.cp(fname, "#{@remark_dir}/ReMarkTest/OUTPUT.INTERNAL")
                end    
            rescue => e
                log "Error:"
                log e.message
            end        
        end
    end    

    def after_run(list_of_files, run_branch, test_branch)
        log "... Copy to temp dir ..."
        copy_to_temp_dir(list_of_files)
        
        GitUtils.undo_git_changes(@remark_dir)

        log ".................. Switch to  Git branch #{test_branch} ..................."
        
        res = system("cd #{@remark_dir} & git checkout -b #{test_branch}")
        if (!res) 
            res = system("cd #{@remark_dir} & git checkout #{test_branch}")
        end

        log "... Copy from temp dir ..."
        copy_from_temp_dir(list_of_files)

        log "... Commit changes ..." 
        results = `cd #{@remark_dir} & git commit -a -m "Test results for #{run_branch} branch"`
        log ("Results: #{results}")

        log "... Push changes ..." 
        results = `cd #{@remark_dir} & git push -u origin #{test_branch}`
        log ("Results: #{results}")
    end

    def get_instance_id_of_this_instance
        metadata_endpoint = 'http://169.254.169.254/latest/meta-data/'
        instance_id = Net::HTTP.get( URI.parse( metadata_endpoint + 'instance-id'))
        return instance_id
    end

    def get_instance_name(instance)
        instance.tags.each do |t|
          if (t["key"] == "Name") then
                return t["value"]
          end
        end 
        return nil
    end 
    
    def copy_files_to_master_node(ip, list_of_files)
        log "Copying files to #{ip} : #{@master_node_dir}"
        list_of_files.each do |f|
            begin
                fname = "#{@remark_dir}/#{f}"
                log "Copy file: #{fname}"
                if f.match(/INPUT\//) then
                    SSHUtils.scp_upload(ip, fname, "#{@master_node_dir}/INPUT")
                end   
                if f.match(/INPUT\.FIXED\//) then
                    SSHUtils.scp_upload(ip, fname, "#{@master_node_dir}/INPUT.FIXED")
                end                              
                if f.match(/OUTPUT\//) then
                    SSHUtils.scp_upload(ip, fname, "#{@master_node_dir}/OUTPUT")
                end
                if f.match(/OUTPUT\.FIXED\//) then
                    SSHUtils.scp_upload(ip, fname, "#{@master_node_dir}/OUTPUT.FIXED")
                end
                if f.match(/OUTPUT\.INTERNAL\//) then
                    SSHUtils.scp_upload(ip, fname, "#{@master_node_dir}/OUTPUT.INTERNAL")
                end    
            rescue => e
                log "Error:"
                log e.message
            end        
        end
    end  

    def get_modified_files(dir)
        arr = GitUtils.get_git_status(dir)
    
        list_of_files = []
        arr.each do |l|
            if (l.match(/(INPUT)/)) then
                list_of_files << l.gsub(/\s*modified\:\s*/, "")
            end         
            if (l.match(/(OUTPUT)/)) then
                list_of_files << l.gsub(/\s*modified\:\s*/, "")
            end
        end
        return list_of_files
    end    
    
    def get_range 
        begin 
            SSHUtils.scp_download(@master_node_ip, "#{@signals_instance_dir}/range", "./")
            line = File.read("./range")
            arr = line.split(" ")
            from_subset = arr[0]
            to_subset = arr[1]
            max_subset = arr[2]    
            return from_subset, to_subset, max_subset
        rescue Exception => e 
            #puts e.full_message(highlight: true, order: :top)
            return nil, nil, nil
        end    
    end

    def get_git_branches
        begin 
            SSHUtils.scp_download(@master_node_ip, "#{@signals_dir}/branch", "./")
            branches = File.read("./branch")
            
            arr = branches.split("\n")

            run_branch = arr[0]
            test_branch = (arr.size > 1) ? arr[1] : nil

            return run_branch, test_branch
        rescue Exception => e 
            #log e.full_message(highlight: true, order: :top)
            return nil, nil 
        end            
    end

    def wait_until_finish(proc_name)

        while (true) do
           res = `tasklist /fi "imagename eq #{proc_name}"`
           return if (res.include?("No tasks"))
           sleep 3 
        end
      
      end     

    def run_all_tests(from_subset, to_subset, max_subset)
        log ("Range: #{from_subset} ... #{to_subset} ... #{max_subset}") 

        from_subset.upto(to_subset) do |i|
            cmd = "cd C:/REMARK_TESTS/AutoStyle/ReMarkTest & start \"Remark tests\" perl TestAutoStyleRM.pl --all --wait_when_finish --force_close --subset #{i} of #{max_subset}"
            log ("Run:#{cmd}")
            system(cmd)
        end

        log "... Wait until tests finished ..."
        wait_until_finish("perl.exe")

        log "... Finished ..."
    end

    def print_list_of_files(list_of_files)
        log "List of processed files"
        log "-----------------------"
        list_of_files.each do |f|
            log "#{f}"
        end                
        log "-----------------------"
    end

    def service_stop
        log 'Ended'
        exit!
    end
      
    def log(text)
      File.open('worker_log.txt', 'a') { |f| f.puts "#{Time.now}: #{text}" }
    end
  
 
    def service_main
        started_flag = 0
    begin    
        while running? do
            #############################
            
           if SSHUtils.check_for_signal(@master_node_ip, "start") && started_flag == 0 then                
                log "... Start ..."
                started_flag = 1

                clear_temp_dirs()

                from_subset, to_subset, max_subset = get_range()
                if (from_subset == nil) then
                    log "Warning: Range was not found"
                    sleep(@polling_time)
                    started_flag = 0
                    next
                else
                    log "Found range: #{from_subset} ... #{to_subset} ... #{max_subset}"    
                end

                log "... Sync with Git repository ..."
                run_branch, test_branch = get_git_branches()

                log "<Run> branch: #{run_branch}"
                log "<Test> branch: #{test_branch}"
                
                #run_branch = "AWS-Tester-1"   
                #test_branch = "AWS-Tester-1-Results"

                if (run_branch == nil)
                    log "Warning: Git branch not found yet ..."
                    sleep(@polling_time)
                    started_flag = 0
                    next                    
                end

                log "........ Use Git branch: #{run_branch}"
                GitUtils.undo_git_changes(@remark_dir)

                log "........ sync_with_git: #{@remark_dir} #{run_branch}"
                GitUtils.sync_with_git(@remark_dir, run_branch)
               
                log "... Run all tests ..."
                run_all_tests(from_subset, to_subset, max_subset)
                
                log "... get_modified_files ..."

                # Get Git diff delta
                list_of_files = get_modified_files(@remark_dir)

                #log "... print_list_of_files ..."
                #print_list_of_files(list_of_files)

                log "... copy files to master node ..."
                                
                after_run(list_of_files, run_branch, test_branch)

                # Copy files from Git diff delta to master node 
                #copy_files_to_master_node(@master_node_ip, list_of_files)

                log "The processed files have been copied"
                                
                log "Send <done> signal"
                results = SSHUtils.execute(@master_node_ip, "cd #{@signals_instance_dir}; touch done")
                started_flag = 0

                clear_temp_dirs()
            end

            #############################
            sleep(@polling_time)
        end
    end


rescue => e
    log e.message
end

end

Worker.mainloop

#w = Worker.new()
#w.run()


